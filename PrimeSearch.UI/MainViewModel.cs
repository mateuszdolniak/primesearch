﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Input;

namespace PrimeSearch.UI
{
    /// <summary>
    /// ViewModel for main window
    /// </summary>
    class MainViewModel : NotifyPropertyChanged
    {
        private uint _numberRange;
        private int _progress;
        private string _resultText;
        private bool _isCalculatingInProgress;

        private CancellationTokenSource _tokenSource;

        /// <summary>
        /// Default constructor
        /// </summary>
        public MainViewModel()
        {
            StartCommand = new DelegateCommand(Start);
            CancelComand = new DelegateCommand(Cancel);
        }

        private void Cancel()
        {
            _tokenSource?.Cancel();
        }

        public ICommand CancelComand { get; }

        /// <summary>
        /// Performs prime number search
        /// </summary>
        private async void Start()
        {
            ResultText = "Operacja w trakcie";
            IsCalculatingInProgress = true;
            Progress = 0;
            using (_tokenSource = new CancellationTokenSource())
            {
                var progress = new Progress<int>(p=>
                {
                    Progress = p;
                });
                var primes = new PrimeSearchService().GetPrimesAsync((int) NumberRange, _tokenSource.Token, progress);
                PrimeNumbers.Clear();
                try
                {
                    PrimeNumbers.AddRange(await primes,
                        ObservableCollectionEx<int>.ECollectionChangeNotificationMode.Reset);
                    ResultText = "Operacja zakończona pomyślnie";
                    Progress = 100;
                }
                catch (OperationCanceledException)
                {
                    PrimeNumbers.Clear();
                    ResultText = "Operacja zakończona pomyślnie";
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message,"Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            IsCalculatingInProgress = false;
            _tokenSource = null;
        }

        /// <summary>
        /// Max Value, where prime numbers should be searched for 
        /// </summary>
        public uint NumberRange
        {
            get => _numberRange;
            set
            {
                _numberRange = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Progress from 0 to 100 of current operation
        /// </summary>
        public int Progress
        {
            get => _progress;
            set
            {
                if (_progress == value)
                {
                    return;
                }
                _progress = value;
                OnPropertyChanged();
            }
        }

        public ObservableCollectionEx<int> PrimeNumbers { get; } = new ObservableCollectionEx<int>();

        public ICommand StartCommand { get; }

        public string ResultText
        {
            get => _resultText;
            set
            {
                _resultText = value;
                OnPropertyChanged();
            }
        }

        public bool IsCalculatingInProgress
        {
            get => _isCalculatingInProgress;
            set
            {
                if (value == _isCalculatingInProgress) return;
                _isCalculatingInProgress = value;
                OnPropertyChanged();
            }
        }
    }
}
