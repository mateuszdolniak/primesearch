﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace PrimeSearch.UI
{
    public class PrimeSearchService
    {
        private bool[] MakeSieve(int max, CancellationToken tokenSourceToken, IProgress<int> progress)
        {
            if(max < 2)
                throw new ArgumentOutOfRangeException(nameof(max), max, "Range end should be at least 2");

            progress = new ProgressDistinctProxy<int>(progress);
            // Make an array indicating whether numbers are prime.
            bool[] isComposite = new bool[max+1];
            isComposite[0] = isComposite[1] = true;

            // Cross out multiples.
            for (int i = 2; i < max; i++)
            {
                // See if i is prime.
                if (!isComposite[i])
                {
                    // Knock out multiples of i.
                    for (int j = i * 2; j <= max; j += i)
                        isComposite[j] = true;
                    // Thread.Sleep(1);

                    //lastValue = i;
                    //numblerFounded++;
                    //Task.Run(() => Console.Write($"\rZnalezionych liczb: {numblerFounded} Ostatnia znaleziona liczba: {lastValue} Postęp: {procent:P}"));
                }
                progress.Report((int)(100*(2.0 * max - i + 1.0) * i / (max * (max + 1.0))));
                tokenSourceToken.ThrowIfCancellationRequested();
            }
            return isComposite;
        }

        public IEnumerable<int> GetPrimes(int max, CancellationToken tokenSourceToken, IProgress<int> progress)
        {
            return MakeSieve(max, tokenSourceToken, progress)
                .Select((isComposite, number) => new {isComposite, number})
                .Where(pair => !pair.isComposite)
                .Select(pair => pair.number);
        }

        public Task<List<int>> GetPrimesAsync(int max, CancellationToken tokenSourceToken, IProgress<int> progress)
        {
            return Task.Run(() => GetPrimes(max, tokenSourceToken, progress).ToList(), tokenSourceToken);
        }
    }

    internal class ProgressDistinctProxy<T> : IProgress<T> where T : struct, IEquatable<T>
    {
        private readonly IProgress<T> _progress;
        private T? _lastValue;

        public ProgressDistinctProxy(IProgress<T> progress)
        {
            _progress = progress; 
        }

        public void Report(T value)
        {
            if (_lastValue == null || !value.Equals(_lastValue ?? default(T)))
            {
                _lastValue = value;
                _progress.Report(value);
            }
        }
    }
}
